from django.conf.urls import url
from . import views


urlpatterns = [
    #127.0.0.1/polls/
    url(r'^$',views.index,name="index"),
    #127.0.0.1/polls/12
    url(r'^(?P<question_id>[0-9]+)/$',views.detail, name='detail'),

]