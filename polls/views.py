from django.shortcuts import render, get_object_or_404
#CARA 1)
from django.http import HttpResponseNotFound
from .models import  Question, Choice

#CARA 2)
#from django.template import loader, RequestContext
# Create your views here.

def index(request):
    latest_Questions = Question.objects.order_by('-pub_date')[:5]
    # CARA 3
    context = {'pertanyaan': latest_Questions}
    return render(request, 'polls/index.html',context)

    #CARA 2) dengan requestContext
    #template = loader.get_template('polls/index.html')
    #context = RequestContext(request, {
    #    'latest_questions': latest_Questions
    #})
    #return HttpResponse(template.render(context))

    #CARA1) tanpa render template
    #output = "<br>".join(q.question_text for q in latest_Questions)
    #return HttpResponse(output)


def detail(request, tanya_id):
    if  not tanya_id:
        return HttpResponseNotFound('<h1>Tidak ditemukan</h1>')
    else:
        detailquestions = get_object_or_404(Question, pk = tanya_id)
        context = {'pertanyaandetail': detailquestions}
        return render(request, 'polls/detail.html', context)



def results(request,question_id):
    #return HttpResponse(' Halaman Results : %s' % question_id)
    return None

def vote(request, question_id):
    #return HttpResponse(' Halaman vote : %s' % question_id)
    return None